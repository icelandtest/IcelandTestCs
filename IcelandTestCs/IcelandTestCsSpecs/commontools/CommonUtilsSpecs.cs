using System.Collections.Generic;
using IcelandTestCs.commontools;
using NUnit.Framework;
using static IcelandTestCs.matchers.Matchers;

namespace IcelandTestCsSpecs.commontools {
    [TestFixture]
    public class CommonUtilsSpecs {
        
       [Test]
       public void ifPresentExecutesAnOperationOnAnObjectOnlyIfItIsNonNull() {
           expectThat(42.ifPresent(it => it + 10).isEqualTo(52));
           
           int? aNullableInt = null;
           expectThat(aNullableInt.ifPresent(it => it + 10).isNull());
           
           // the null-coalescing-operator can be used to write an "else"-statement
           expectThat((aNullableInt.ifPresent(it => it + 10) ?? 42).isEqualTo(42));

           // this can be used in a different context as well
           // given
           var results = new List<string?>();
           // when
           "otto".ifPresent(it => results.Add(it));
           (null as string).ifPresent(it => results.Add(it));
           "hugo".ifPresent(it => results.Add(it));
           // then
           expectThat(results.hasElements(new List<string>{"otto", "hugo"}));
       }
    }
}