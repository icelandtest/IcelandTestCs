using IcelandTestCs.extensions;
using NUnit.Framework;

namespace IcelandTestCsSpecs.extensions {
    
    [TestFixture]
    public class EnumerableExtensionsSpec {

        [Test]
        public void theIndexOfTheFirstDifferenceBetweenTwoEnumerablesCanBeObtained() {
            Assert.That(new[] {1, 2, 3}.idxOfFirstDifferenceTo(new[]{1, 2, 3}), Is.EqualTo(EnumerableExtensions.NoDifference));

            Assert.That(new[] {0, 2, 3}.idxOfFirstDifferenceTo(new[]{1, 2, 3}), Is.EqualTo(0));
            Assert.That(new[] {1, 0, 3}.idxOfFirstDifferenceTo(new[]{1, 2, 3}), Is.EqualTo(1));
            Assert.That(new[] {1, 2, 0}.idxOfFirstDifferenceTo(new[]{1, 2, 3}), Is.EqualTo(2));
            
            Assert.That(new[] {1, 2, 3}.idxOfFirstDifferenceTo(new[]{1, 2}), Is.EqualTo(EnumerableExtensions.LhsIsLonger));
            Assert.That(new[] {1, 2}.idxOfFirstDifferenceTo(new[]{1, 2, 3}), Is.EqualTo(EnumerableExtensions.RhsIsLonger));
            
            Assert.That(new[] {1, 2, 3}.idxOfFirstDifferenceTo(new int[]{}), Is.EqualTo(EnumerableExtensions.LhsIsLonger));
            Assert.That(new int[] {}.idxOfFirstDifferenceTo(new[]{1, 2, 3}), Is.EqualTo(EnumerableExtensions.RhsIsLonger));
        }
        
        [Test]
        public void areAllEqualToIsSupportedForTwoEnumerables() {
            Assert.That(new[] {1, 2, 3}.areAllEqualTo(new[]{1, 2, 3}), Is.True);

            Assert.That(new[] {0, 2, 3}.areAllEqualTo(new[]{1, 2, 3}), Is.False);
            Assert.That(new[] {1, 0, 3}.areAllEqualTo(new[]{1, 2, 3}), Is.False);
            Assert.That(new[] {1, 2, 0}.areAllEqualTo(new[]{1, 2, 3}), Is.False);
            
            Assert.That(new[] {1, 2, 3}.areAllEqualTo(new[]{1, 2}), Is.False);
            Assert.That(new[] {1, 2}.areAllEqualTo(new[]{1, 2, 3}), Is.False);
            
            Assert.That(new[] {1, 2, 3}.areAllEqualTo(new int[]{}), Is.False);
            Assert.That(new int[] {}.areAllEqualTo(new[]{1, 2, 3}), Is.False);
        }
        
    }
}