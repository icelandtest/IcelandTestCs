using System;
using System.Collections.Generic;
using IcelandTestCs.exceptions;
using NUnit.Framework;
using static IcelandTestCs.matchers.Matchers; // this makes the matcher funcs visible...

namespace IcelandTestCsSpecs.examples; 

// in the following there are small examples how to use the matchers
// for a detailed specification of the behaviour of the matchers please have
// a look at their specs which can be found in iceland/matchers/MatchersSpecs.kt in the test-module

[TestFixture]
public class UsingMatchersToWriteSpecs {

    static class Something {
        public static int doSomething(int value) => value < 0 ? throw new InvalidOperationException("oops") : value + 10;
    }

    [Test]
    public void failIsUsedToForceAnUnconditionalFailOfASpec() {
        try {
            Something.doSomething(-2);
            fail("expected exception not thrown");
        } catch (InvalidOperationException exc) {
            // we want to end up here :-)
        }
    }

    [Test]
    public void thereAreThreeWaysToUseTheMatchers() {
        // the first way is to use expectThat
        // this is the standard for writing specs - it makes the spec fail
        // if an expectation is not met
        expectThat((1 == 1).isTrue()); // the whole matcher-expression is written inside the braces of expectThat(...)
        
        // the second way is to use isExpectationMet
        // this one simply tells through a boolean return value if an expectation is met
        // and can e.g. be quite useful when using matchers in production code for
        // testing user-input or something like that
        var isItOk = isExpectationMet((1 == 1).isTrue());
        if (!isItOk)
            fail();
        
        // the third way is to use whichFailDoWeGetFor
        // this one returns a nullable string being null if everything was ok
        // or containing an appropriate error message if an expectation is not met
        var possibleErrorMsg = whichFailDoWeGetFor((1 == 1).isTrue());
        if (possibleErrorMsg is not null)
            fail("we should have gotten an error message");
    }

    [Test]
    public void weCanWriteChecksForBooleanValuesUsingIsFalseOrIsTrue() {
        expectThat((1 % 2 == 0).isFalse());
        expectThat((4 % 2 == 0).isTrue());
    }

    [Test]
    public void weCanWriteChecksForNullValuesUsingIsNullOrIsNotNull() {
        expectThat(((string?)null).isNull());
        expectThat("whatever".isNotNull());
    }

    [Test]
    public void weCanWriteChecksForEqualityUsingIsEqualToOrIsNotEqualTo() {
        expectThat(1.isEqualTo(1));
        expectThat(1.isNotEqualTo(2));
    }

    [Test]
    public void weCanWriteChecksForClosenessForDoubles() {
        expectThat((1.0).isCloserTo(1.1).than(0.5));
        expectThat(
            isExpectationMet((1.0).isCloserTo(1.1).than(0.1)).isFalse() // the epsilon itself is excluded!
            );
    }

    [Test]
    public void weCanWriteChecksForReferenceEqualityUsingIsSameAsOrIsNotSameAs() {
        var justAString = "whateverXXX".Substring(0, 8);
        var justAnotherString = "whateverXXX".Substring(0, 8);
        
        expectThat(justAString.isSameAs(justAString));
        expectThat(justAString.isNotSameAs(justAnotherString));
    }

    [Test]
    public void weCanWriteChecksForOrderingRelationsIfTheParticipantsInTheChecksHaveComparableTypes() {
        expectThat(1.isLessThan(2));
        expectThat(2.isGreaterThan(1));
        expectThat(1.isLessThanOrEqualTo(1));
        expectThat(1.isGreaterThanOrEqualTo(1));
    }

    [Test]
    public void weCanWriteChecksForExpectedExceptions() {
        expectThat(new Action(() => Something.doSomething(-1))
            .throws(exc => exc is InvalidOperationException));
        
        expectThat(new Action(() => Something.doSomething(-1))
            .throws(exc => // if we want to, we can write more detailed specs as well
                exc is InvalidOperationException &&
                exc.Message == "oops"
            ));
    }

    [Test]
    public void weCanWriteChecksForEqualityOfEnumerables() {
        var aList = new List<int> { 1, 2, 3 };
        expectThat(aList.hasElements(new[] { 1, 2, 3}));
    }
}