using System.Collections.Generic;
using IcelandTestCs.datastructure;
using NUnit.Framework;
using static IcelandTestCs.matchers.Matchers;


namespace IcelandTestCsSpecs.datastructure {
    [TestFixture]
    public class EnumerableHelpersSpecs {

        [Test]
        public void theIndexOfTheFirstDifferenceOfTwoIterablesCanBeObtained() {
            expectThat(new List<int> {1, 2, 3}.idxOfFirstDifferenceTo(new List<int> {1, 2, 3})
                .isEqualTo((int)DifferenceIdx.NoDifference));
            
            expectThat(new List<int> {1, 0, 3}.idxOfFirstDifferenceTo(new List<int> {1, 2, 3})
                .isEqualTo(1));
            expectThat(new List<int> {1, 2, 0}.idxOfFirstDifferenceTo(new List<int> {1, 2, 3})
                .isEqualTo(2));
            
            expectThat(new List<int> {1, 2}.idxOfFirstDifferenceTo(new List<int> {1, 2, 3})
                .isEqualTo((int)DifferenceIdx.RhsIsLonger));
            expectThat(new List<int> {1, 2, 3}.idxOfFirstDifferenceTo(new List<int> {1, 2})
                .isEqualTo((int)DifferenceIdx.LhsIsLonger));
            
            expectThat(new List<int> {1, 2, 3}.idxOfFirstDifferenceTo(new List<int>())
                .isEqualTo((int)DifferenceIdx.LhsIsLonger));
            expectThat(new List<int>().idxOfFirstDifferenceTo(new List<int> {1, 2, 3})
                .isEqualTo((int)DifferenceIdx.RhsIsLonger));
            
            // custom equality-comparison is supported too
            expectThat(new List<string> {"abc", "def", "ghi"}.idxOfFirstDifferenceTo(
                    new List<string> {"axx", "dxx", "gxx"},
                    (lhs, rhs) => lhs[0] == rhs[0])
                .isEqualTo((int)DifferenceIdx.NoDifference));
        }
    }
}