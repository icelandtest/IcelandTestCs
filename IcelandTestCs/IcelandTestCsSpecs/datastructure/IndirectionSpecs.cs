using System;
using IcelandTestCs.datastructure;
using IcelandTestCs.exceptions;
using NUnit.Framework;
using static IcelandTestCs.matchers.Matchers;

namespace IcelandTestCsSpecs.datastructure {
    [TestFixture]
    public class IndirectionSpecs {
        
        [Test]
        public void indirectionsAreCreatedWithANullableDestination() {
            // given
            var target0 = new Indirection<int>();
            var target1 = new Indirection<int>(42);

            // then
            expectThat(target0.hasDst.isFalse());
            expectThat(new Action(() => { var dummy = target0.dst; })
                .throws(it => it is NotAvailableException));

            expectThat(target1.hasDst.isTrue());
            expectThat(target1.dst.isEqualTo(42));
        }
    }
}