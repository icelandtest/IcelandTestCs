﻿using System;
using System.Collections.Generic;
using IcelandTestCs.datastructure;
using IcelandTestCs.exceptions;
using NUnit.Framework;
using static IcelandTestCs.matchers.Matchers;

namespace IcelandTestCsSpecs.matchers {
    
    [TestFixture]
    public class MatchersSpecs {
        
        [Test]
        public void failCanBeForcedInSpecs() {
            var reachedLineThatShouldNotBeReached = false;

            try {
                fail("message");
                // the compiler detects this as unreachable, but just for paranoia reasons
                // (=somebody changes the implementation of fail()) we leave it here
                reachedLineThatShouldNotBeReached = true; 
            } catch(ExpectationNotMetException exc) {
                if (exc.Message != "message")
                    throw new ExpectationNotMetException("expected message is not contained in the expected exception");
            }

            if (reachedLineThatShouldNotBeReached)
                throw new ExpectationNotMetException(
                    "a call to fail(\"message\") did not throw the expected exception");
        }
        
        [Test]
        public void weCanAskWhetherAnExpectationIsMet() {
            if (!isExpectationMet(null))
                fail("a null value should indicate that an expectation is met");
            if (isExpectationMet("this is an error message"))
                fail("a non-null value should indicate that an expectation is not met");
        }
        
        [Test]
        public void weCanAskWhichFailWeGetForAnExpectationIfAny() {
            if (whichFailDoWeGetFor(null) is not null)
                fail("a null value should indicate that an expectation is met");
            if (whichFailDoWeGetFor("this is an error message") != "this is an error message")
                fail("a non-null value should indicate that an expectation is not met");
        }
        
        [Test]
        public void expectThrowsIfAnExpectationIsNotMet() {
            expectThat(null); // this one does not throw, because there is no error-message

            try {
                expectThat("expectation was not met");
                fail("expectThat did not throw although a mismatch was passed");
            } catch (ExpectationNotMetException exc) {
                // we want to end up here :-)
                if (exc.Message != "expectation was not met")
                    fail("expected assertion-error message is wrong");
            } catch (Exception exc) {
                fail($"unexpected exception thrown {exc}");
            }
        }
       
        [Test]
        public void isEqualToChecksForEquality() {
            expectThat(1.isEqualTo(1));

            var result = 1.isEqualTo(2);
            if (result == null)
                fail("isEqualTo says that two non-equal values would be equal");
            if (result != "Objects are not equal, expected: <2>, but obtained: <1>")
                fail("unexpected error-message");
        }
        
        [Test]
        public void isNotEqualToChecksForUnequality() {
            expectThat(1.isNotEqualTo(2));

            var result = 1.isNotEqualTo(1);
            if (result == null)
                fail("isNotEqualTo says that two equal values would be non-equal");
            if (result != "Objects are equal, although they should not be: <1>")
                fail("unexpected error-message");
        }
        
        [Test]
        public void isSameAsChecksForReferenceIdentity() {
            var obj1 = new Indirection<int>(1);
            var obj2 = new Indirection<int>(1);

            expectThat(obj1.isSameAs(obj1));
            var result = obj1.isSameAs(obj2);
            expectThat(result.isEqualTo(
                "Objects are not the same, expected: <Indirection[hasDst = false]>, " +
                "but obtained: <Indirection[hasDst = false]>"));
            // just to show that it's really about reference equality - the two Indirections compare equal!
            expectThat(obj1.isEqualTo(obj2));
        }
        
        [Test]
        public void isNotSameAsChecksForReferenceNonIdentity() {
            var obj1 = new Indirection<int>(1);
            var obj2 = new Indirection<int>(1);

            expectThat(obj1.isNotSameAs(obj2));
            var result = obj1.isNotSameAs(obj1);
            expectThat(result.isEqualTo(
                "Objects are the same, although they should not be: <Indirection[hasDst = false]>"));
            // just to show that it's really about reference equality - the two Indirections compare equal!
            expectThat(obj1.isEqualTo(obj2));
        }
        
        [Test]
        public void isTrueChecksForAValueBeingTrue() {
            expectThat(true.isTrue());
            var result = false.isTrue();
            expectThat(result.isEqualTo("Value is not true"));
        }
        
        [Test]
        public void isFalseChecksForAValueBeingFalse() {
            expectThat(false.isFalse());
            var result = true.isFalse();
            expectThat(result.isEqualTo("Value is not false"));
        }

        [Test]
        public void isNullChecksForAValueBeingNull() {
            string? str = null;
            expectThat(str.isNull());
            var result = 1.isNull();
            expectThat(result.isEqualTo("Value is not null"));
        }
        
        [Test]
        public void isNotNullChecksForAValueNotBeingNull() {
            expectThat(1.isNotNull());
            string? str = null;
            var result = str.isNotNull();
            expectThat(result.isEqualTo("Value is null"));
        }

        [Test]
        public void isLessThanIsSupportedForComparables() {
            var result = 1.isLessThan(2);
            expectThat(result.isNull());

            result = 1.isLessThan(1);
            expectThat(result.isEqualTo("<1> is not less than <1>"));
           
            result = 2.isLessThan(1);
            expectThat(result.isEqualTo("<2> is not less than <1>"));

            result = "abc".isLessThan("abd");
            expectThat(result.isNull());
            
            result = "abc".isLessThan("abc");
            expectThat(result.isEqualTo("<abc> is not less than <abc>"));
            
            result = "abd".isLessThan("abc");
            expectThat(result.isEqualTo("<abd> is not less than <abc>"));

            string? str = null;
            result = str.isLessThan("abc");
            expectThat(result.isEqualTo("Obtained comparable is null"));

            result = "abc".isLessThan(str);
            expectThat(result.isEqualTo("Expected comparable is null"));
        }
        
        [Test]
        public void isLessThanOrEqualToIsSupportedForComparables() {
            var result = 1.isLessThanOrEqualTo(2);
            expectThat(result.isNull());

            result = 1.isLessThanOrEqualTo(1);
            expectThat(result.isNull());
           
            result = 2.isLessThanOrEqualTo(1);
            expectThat(result.isEqualTo("<2> is not less than or equal to <1>"));

            result = "abc".isLessThanOrEqualTo("abd");
            expectThat(result.isNull());
            
            result = "abc".isLessThanOrEqualTo("abc");
            expectThat(result.isNull());
            
            result = "abd".isLessThanOrEqualTo("abc");
            expectThat(result.isEqualTo("<abd> is not less than or equal to <abc>"));

            string? str = null;
            result = str.isLessThanOrEqualTo("abc");
            expectThat(result.isEqualTo("Obtained comparable is null"));

            result = "abc".isLessThanOrEqualTo(str);
            expectThat(result.isEqualTo("Expected comparable is null"));
        }
        
        [Test]
        public void isGreaterThanIsSupportedForComparables() {
            var result = 2.isGreaterThan(1);
            expectThat(result.isNull());

            result = 2.isGreaterThan(2);
            expectThat(result.isEqualTo("<2> is not greater than <2>"));
           
            result = 1.isGreaterThan(2);
            expectThat(result.isEqualTo("<1> is not greater than <2>"));

            result = "abd".isGreaterThan("abc");
            expectThat(result.isNull());
            
            result = "abc".isGreaterThan("abc");
            expectThat(result.isEqualTo("<abc> is not greater than <abc>"));
            
            result = "abc".isGreaterThan("abd");
            expectThat(result.isEqualTo("<abc> is not greater than <abd>"));

            string? str = null;
            result = str.isGreaterThan("abc");
            expectThat(result.isEqualTo("Obtained comparable is null"));

            result = "abc".isGreaterThan(str);
            expectThat(result.isEqualTo("Expected comparable is null"));
        }
        
        [Test]
        public void isGreaterThanOrEqualToIsSupportedForComparables() {
            var result = 2.isGreaterThanOrEqualTo(1);
            expectThat(result.isNull());

            result = 1.isGreaterThanOrEqualTo(1);
            expectThat(result.isNull());
           
            result = 1.isGreaterThanOrEqualTo(2);
            expectThat(result.isEqualTo("<1> is not greater than or equal to <2>"));

            result = "abd".isGreaterThanOrEqualTo("abc");
            expectThat(result.isNull());
            
            result = "abc".isGreaterThanOrEqualTo("abc");
            expectThat(result.isNull());
            
            result = "abc".isGreaterThanOrEqualTo("abd");
            expectThat(result.isEqualTo("<abc> is not greater than or equal to <abd>"));

            string? str = null;
            result = str.isGreaterThanOrEqualTo("abc");
            expectThat(result.isEqualTo("Obtained comparable is null"));

            result = "abc".isGreaterThanOrEqualTo(str);
            expectThat(result.isEqualTo("Expected comparable is null"));
        }
        
        [Test]
        public void checkingForEpsilonRangesIsSupportedForDoubles() {
            expectThat((1.0).isCloserTo(1.0).than(1e-5));
            expectThat((1.0).isCloserTo(1.2).than(0.3));
            expectThat((1.2).isCloserTo(1.0).than(0.3));
            expectThat((-1.0).isCloserTo(-1.2).than(0.3));
            expectThat((-1.2).isCloserTo(-1.0).than(0.3));
            expectThat((1.0).isCloserTo(1.2).than(-0.3)); // negative epsilons are considered valid

            var result = (1.0).isCloserTo(1.2).than(0.1);
            expectThat(result.isEqualTo("<1> is not within an epsilon-range of <0.1> to <1.2>"));
            
            result = (-1.0).isCloserTo(-1.2).than(0.1);
            expectThat(result.isEqualTo("<-1> is not within an epsilon-range of <0.1> to <-1.2>"));
            
            result = (-1.0).isCloserTo(-1.2).than(-0.1);
            expectThat(result.isEqualTo("<-1> is not within an epsilon-range of <-0.1> to <-1.2>"));
        }
        
        [Test]
        public void throwsChecksIfAnExceptionIsThrownAndMeetsAGivenCondition() {
            expectThat(new Action(() => { throw new NotAvailableException(); })
                .throws(exc => exc is NotAvailableException));
            
            var result = new Action(() => { })
                .throws(exc => true);
            expectThat(result.isEqualTo("No exception was thrown at all"));

            result = new Action(() => { throw new NotAvailableException(); })
                .throws(exc => exc is ExpectationNotMetException);
            expectThat(result.isEqualTo("Exception condition not met"));
        }
        
        [Test]
        public void hasElementsCanBeUsedToCheckEnumerables() {
            expectThat(new List<int> {1, 2, 3, 4, 5}.hasElements(new List<int> {1, 2, 3, 4, 5}));

            var queue = new Queue<int>();
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);
            queue.Enqueue(4);
            queue.Enqueue(5);
            expectThat(queue.hasElements(new List<int> {1, 2, 3, 4, 5}));

            var result = new List<int> {1, 2, 3, 4, 5}.hasElements(new List<int> {1, 2, 3, 4});
            expectThat(result.isEqualTo("Enumerables do not match: obtained longer than expected"));
            
            result = new List<int> {1, 2, 3, 4}.hasElements(new List<int> {1, 2, 3, 4, 5});
            expectThat(result.isEqualTo("Enumerables do not match: obtained shorter than expected"));

            result = new List<int> {1, 2, 8, 4, 5}.hasElements(new List<int> {1, 2, 3, 4, 5});
            expectThat(result.isEqualTo("Enumerables do not match: first difference found at index 2"));
        }
    }
    
}