namespace IcelandTestCs.extensions {
    
    public static class EnumerableExtensions {
        
        public const int NoDifference = -1;
        public const int LhsIsLonger = -2;
        public const int RhsIsLonger = -3;
        public static int idxOfFirstDifferenceTo<TObj>(this IEnumerable<TObj> lhs, IEnumerable<TObj> rhs) {
            var idx = 0;
            using var lhsEnumerator = lhs.GetEnumerator();
            using var rhsEnumerator = rhs.GetEnumerator();
            var hasLhsValue = lhsEnumerator.MoveNext();
            var hasRhsValue = rhsEnumerator.MoveNext();

            while (hasLhsValue) {
                if (!hasRhsValue)
                    return LhsIsLonger;
                if (!lhsEnumerator.Current!.Equals(rhsEnumerator.Current))
                    return idx;
                ++idx;
                hasLhsValue = lhsEnumerator.MoveNext();
                hasRhsValue = rhsEnumerator.MoveNext();
            }

            return hasRhsValue ? RhsIsLonger : NoDifference;
        }

        public static bool areAllEqualTo<TObj>(this IEnumerable<TObj> lhs, IEnumerable<TObj> rhs) {
            return lhs.idxOfFirstDifferenceTo(rhs) == NoDifference;
        }

    }
}