using IcelandTestCs.commontools;
using IcelandTestCs.datastructure;
using IcelandTestCs.exceptions;
using IcelandTestCs.extensions;

namespace IcelandTestCs.matchers {
   
   public static class Matchers {
        public static void fail(string? msg = null) { throw new ExpectationNotMetException(msg ?? ""); }
        
        public static bool isExpectationMet(string? errorMsg) => errorMsg == null;

        public static string? whichFailDoWeGetFor(string? errorMsg) => errorMsg;
        
        public static void expectThat(string? errorMsg) {
            errorMsg.ifPresent<string?, string?>(dummy => 
                throw new ExpectationNotMetException(errorMsg));
        }

        public static string? isEqualTo(this object? obtained, object? expected) =>
            Equals(obtained, expected)
                ? null
                : $"Objects are not equal, expected: <{expected}>, but obtained: <{obtained}>";

        public static string? isNotEqualTo(this object? obtained, object? expected) =>
            Equals(obtained, expected)
                ? $"Objects are equal, although they should not be: <{expected}>"
                : null;
        
        public static string? isSameAs(this object? obtained, object? expected) =>
            ReferenceEquals(obtained, expected)
                ? null
                : $"Objects are not the same, expected: <{expected}>, but obtained: <{obtained}>";
        
        public static string? isNotSameAs(this object? obtained, object? expected) =>
            ReferenceEquals(obtained, expected)
                ? $"Objects are the same, although they should not be: <{expected}>"
                : null;

        public static string? isTrue(this bool value) => value ? null : "Value is not true";
        public static string? isFalse(this bool value) => value ? "Value is not false" : null;
        public static string? isNull(this object? value) => value == null ? null : "Value is not null";
        public static string? isNotNull(this object? value) => value == null ? "Value is null" : null;

        public static string? isLessThan<TValue>(this TValue? obtained, TValue? expected)
            where TValue : IComparable<TValue> {
            if (obtained == null) return "Obtained comparable is null";
            if (expected == null) return "Expected comparable is null";
            if (obtained.CompareTo(expected) < 0) return null;
            return $"<{obtained}> is not less than <{expected}>";
        }
        
        public static string? isLessThanOrEqualTo<TValue>(this TValue? obtained, TValue? expected)
            where TValue : IComparable<TValue> {
            if (obtained == null) return "Obtained comparable is null";
            if (expected == null) return "Expected comparable is null";
            if (obtained.CompareTo(expected) <= 0) return null;
            return $"<{obtained}> is not less than or equal to <{expected}>";
        }
        
        public static string? isGreaterThan<TValue>(this TValue? obtained, TValue? expected)
            where TValue : IComparable<TValue> {
            if (obtained == null) return "Obtained comparable is null";
            if (expected == null) return "Expected comparable is null";
            if (obtained.CompareTo(expected) > 0) return null;
            return $"<{obtained}> is not greater than <{expected}>";
        }
        
        public static string? isGreaterThanOrEqualTo<TValue>(this TValue? obtained, TValue? expected)
            where TValue : IComparable<TValue> {
            if (obtained == null) return "Obtained comparable is null";
            if (expected == null) return "Expected comparable is null";
            if (obtained.CompareTo(expected) >= 0) return null;
            return $"<{obtained}> is not greater than or equal to <{expected}>";
        }

        public class ObtainedAndExpectedForEpsilonMatch {
            private double obtained { get; }
            private double expected { get; }

            public ObtainedAndExpectedForEpsilonMatch(double obtained, double expected) {
                this.obtained = obtained;
                this.expected = expected;
            }

            public string? than(double epsilon) {
                var absDiff = Math.Abs(obtained - expected);
                var absEpsilon = Math.Abs(epsilon);
                if (absDiff >= absEpsilon)
                    return $"<{obtained}> is not within an epsilon-range of <{epsilon}> to <{expected}>";
                return null;
            }
        }

        public static ObtainedAndExpectedForEpsilonMatch isCloserTo(this double obtained, double expected) =>
            new ObtainedAndExpectedForEpsilonMatch(obtained, expected);

        public static string? throws(this Action doThat, Func<Exception, bool> exceptionMeetsCondition) {
            try {
                doThat();
            }
            catch (Exception exc) {
                return exceptionMeetsCondition(exc) ? null : "Exception condition not met";
            }
            return "No exception was thrown at all";
        }

        public static string? hasElements<TElement>(
            this IEnumerable<TElement> obtained,
            IEnumerable<TElement> expected) {
            var idxOfFirstDifference = EnumerableExtensions.idxOfFirstDifferenceTo(obtained, expected);
            if (idxOfFirstDifference == (int) DifferenceIdx.NoDifference) return null;
            if (idxOfFirstDifference == (int) DifferenceIdx.LhsIsLonger) 
                return "Enumerables do not match: obtained longer than expected";
            if (idxOfFirstDifference == (int) DifferenceIdx.RhsIsLonger) 
                return "Enumerables do not match: obtained shorter than expected";
            return $"Enumerables do not match: first difference found at index {idxOfFirstDifference}";
        }
     
   }
   
}