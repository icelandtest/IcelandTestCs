namespace IcelandTestCs.commontools {
    public static class CommonUtils {
        
        public static TResult? ifPresent<TObj, TResult>(this TObj obj, Func<TObj, TResult> apply) {
            if (obj != null)
                return apply(obj);
            return default;
        }
        
        public static void ifPresent<TObj>(this TObj obj, Action<TObj> apply) {
            if (obj != null)
                apply(obj);
        }
        
    }
}