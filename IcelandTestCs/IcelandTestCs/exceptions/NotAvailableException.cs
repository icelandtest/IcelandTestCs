using System.Runtime.Serialization;

namespace IcelandTestCs.exceptions {
    
    public class NotAvailableException : Exception {
        public NotAvailableException() { }
        public NotAvailableException(string message) : base(message) { }
        public NotAvailableException(string message, Exception inner) : base(message, inner) { }
        protected NotAvailableException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}