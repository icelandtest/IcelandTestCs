using System.Runtime.Serialization;

namespace IcelandTestCs.exceptions {
    
    public class ExpectationNotMetException : Exception {
        public ExpectationNotMetException() { }
        public ExpectationNotMetException(string? message) : base(message) { }
        public ExpectationNotMetException(string? message, Exception inner) : base(message, inner) { }
        protected ExpectationNotMetException(
            SerializationInfo info, 
            StreamingContext context) : base(info, context) { }
    }
}