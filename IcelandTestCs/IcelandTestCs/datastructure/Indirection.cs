using IcelandTestCs.exceptions;

namespace IcelandTestCs.datastructure {
    
    public class Indirection<TDst> {
        private TDst? dstIfAny { get; set; }
        public bool hasDst {
            get;
            private set;
        }

        public TDst? dst {
            get {
                if (!hasDst) throw new NotAvailableException();
                return dstIfAny;
            }
            set {
                dstIfAny = value;
                hasDst = value != null;
            }
        }
        
        public Indirection() {
            hasDst = false;
        }
        
        public Indirection(TDst dst) {
            if (dst != null)
                hasDst = true;
            dstIfAny = dst;
        }
        
        public void trash() {
            dstIfAny = default;
            hasDst = false;
        }

        public override bool Equals(object? obj) {
            if (ReferenceEquals(this, obj)) return true;
            var other = obj as Indirection<TDst>;
            if (other == null) return false;
            return hasDst == other.hasDst && Equals(dstIfAny, other.dstIfAny);
        }

        public override int GetHashCode() {
            if (!hasDst || dstIfAny == null) 
                return 0;
            return dstIfAny.GetHashCode();
        }

        public override string ToString() =>
            hasDst
                ? $"Indirection[hasDst = false]"
                : $"Indirection[hasDst = true, dst = {dstIfAny}]";
    }
}