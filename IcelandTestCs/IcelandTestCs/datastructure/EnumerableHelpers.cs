namespace IcelandTestCs.datastructure {

    public enum DifferenceIdx {
        NoDifference = -1,
        LhsIsLonger = -2,
        RhsIsLonger = -3
    }

    public static class EnumerableHelpers {
        public static int idxOfFirstDifferenceTo<TElement>(this IEnumerable<TElement> lhs, IEnumerable<TElement> rhs) =>
            idxOfFirstDifferenceTo(lhs, rhs, (element0, element1) => Equals(element0, element1));
        
        public static int idxOfFirstDifferenceTo<TElement>(this IEnumerable<TElement> lhs, IEnumerable<TElement> rhs,
            Func<TElement, TElement, bool> areEqual) {
            var lhsIterator = lhs.GetEnumerator();
            var rhsIterator = rhs.GetEnumerator();
            var idx = 0;

            while (lhsIterator.MoveNext()) {
                if (!rhsIterator.MoveNext())
                    return (int)DifferenceIdx.LhsIsLonger;
                if (!areEqual(lhsIterator.Current, rhsIterator.Current))
                    return idx;
                ++idx;
            }
            return rhsIterator.MoveNext() ? (int) DifferenceIdx.RhsIsLonger : (int) DifferenceIdx.NoDifference;
        }
    }
}